#!/bin/bash


mkdir workdir
mkdir workdirXML
mkdir devrepo
mkdir outputdir
mkdir ceirepo

cd devrepo
git init 
git remote add origin https://adityamanocha@bitbucket.org/adityamanocha/temin.git

git fetch

git pull origin develop

git checkout develop

git branch --set-upstream-to=origin/develop develop

cd ..

cd ceirepo
git init
git remote add origin https://adityamanocha@bitbucket.org/adityamanocha/ceidepo.git

git fetch

git pull origin feature

git checkout feature

git branch --set-upstream-to=origin/feature feature

cd ..
